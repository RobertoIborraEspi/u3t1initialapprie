package dam.android.roberto.u3t1initialappejercicios;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class U3T1InitialAppActivity extends AppCompatActivity implements OnClickListener {

    private int count;
    private TextView tvDisplay;
    private Button buttonIncrease, buttonIncrease2, buttonDecrease, buttonDecrease2, buttonReset;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_u3_t1_initial_app);
        setUI();
        buttonIncrease.setOnClickListener(this);
        buttonIncrease2.setOnClickListener(this);
        buttonDecrease.setOnClickListener(this);
        buttonDecrease2.setOnClickListener(this);
        buttonReset.setOnClickListener(this);
    }

    private void setUI() {
        tvDisplay = findViewById(R.id.tvDisplay);
        buttonIncrease = findViewById(R.id.buttonIncrease);
        buttonIncrease2 = findViewById(R.id.buttonIncrease2);
        buttonDecrease = findViewById(R.id.buttonDecrease);
        buttonDecrease2 = findViewById(R.id.buttonDecrease2);
        buttonReset = findViewById(R.id.buttonReset);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.buttonIncrease: count++; break;
            case R.id.buttonIncrease2: count += 2; break;
            case R.id.buttonDecrease: count--; break;
            case R.id.buttonDecrease2: count -= 2; break;
            case R.id.buttonReset: count = 0; break;
        }
        tvDisplay.setText(getString(R.string.number_of_elements) + ": " + count);
    }
    //TODO: Ej3 Restore info on rotate
    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("text", tvDisplay.getText().toString());
        outState.putInt("count", count);
    }

    @Override
    public void onRestoreInstanceState(@Nullable Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        tvDisplay.setText(savedInstanceState.getString("text"));
        count = savedInstanceState.getInt("count");
    }
}